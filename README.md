# Illustration of LRU LFU Caches


GUI for demonstration of work LRU and LRU Caches. Realized by Python modules PyQt6 and cachetools (https://github.com/tkem/cachetools).

## Running

Clone repository to your directory. And run app with the following command:

```
cd lru-lfu-caches
python mainwindow.py
```

